#!/bin/bash

SOFT_DIR="../soft"
CALITAS_DIR=$SOFT_DIR"/calitas"
CASOFFINDER_DIR=$SOFT_DIR"/cas-offinder"

#Check and make soft directory
if [ -d $SOFT_DIR ]
then
  rm -r $SOFT_DIR
fi

mkdir $SOFT_DIR

#Get CRISPRITZ
curl -o $SOFT_DIR/crispritz/auto_test_crispritz_docker.sh --create-dirs https://raw.githubusercontent.com/pinellolab/CRISPRitz/master/test_scripts/auto_test_crispritz_docker.sh

sed -e 's+test_crispritz+../soft/crispritz/test_crispritz+g' $SOFT_DIR/crispritz/auto_test_crispritz_docker.sh > \
        $SOFT_DIR/crispritz/auto_test_crispritz_docker.sh.tmp && \
        mv $SOFT_DIR/crispritz/auto_test_crispritz_docker.sh.tmp $SOFT_DIR/crispritz/auto_test_crispritz_docker.sh

bash $SOFT_DIR/crispritz/auto_test_crispritz_docker.sh

#Get Calitas
if [ -d $CALITAS_DIR ]
then
  rm -r $CALITAS_DIR
fi

mkdir $CALITAS_DIR

wget -O $CALITAS_DIR/calitas-1.0.jar -x https://github.com/editasmedicine/calitas/releases/download/v1.0/calitas-1.0.jar

java -Xmx8g -jar $CALITAS_DIR/calitas-1.0.jar

#Get Cas-OFFinder Binary
if [ -d $CASOFFINDER_DIR ]
then
  rm -r $CASOFFINDER_DIR
fi

mkdir $CASOFFINDER_DIR

wget -O $CASOFFINDER_DIR/cas-offinder https://sourceforge.net/projects/cas-offinder/files/Binaries/2.4/Linux64/cas-offinder/download

chmod a+x "$CASOFFINDER_DIR/cas-offinder"

$CASOFFINDER_DIR/./cas-offinder