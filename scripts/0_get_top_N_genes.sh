#!/bin/bash

NCBI="ncbi"
mkdir $NCBI
wget -N -P $NCBI ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene2pubmed.gz; \
gunzip $NCBI/gene2pubmed.gz;

# filter out only to human
cat $NCBI/gene2pubmed | awk '{if ($1 == 9606) print;}' > $NCBI/gene2pubmed_hg.txt

# get gene info
wget -N -P $NCBI/ ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene_info.gz; \
gunzip $NCBI/gene_info.gz;

cat $NCBI/gene_info | awk '{if ($1 == 9606) print;}' > $NCBI/gene_info_hg.txt

# summarize
Rscript --vanilla parse_top_N_genes.R

rm -rf $NCBI

