# re-downloaded and processed from below links to accommodate gggenome
cd ../genome
wget -O hg38v34.gtf.gz ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_34/gencode.v34.annotation.gtf.gz
wget -O hg38v34.fa.gz ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_34/GRCh38.p13.genome.fa.gz
gunzip *.gz
