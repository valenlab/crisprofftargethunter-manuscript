using BioSequences
using CSV
using DataFrames
using FASTX
using CRISPRofftargetHunter

genome = "/home/ai/Projects/uib/crispr/chopchop_genomes/hg38v34.fa"
motif = Motif("Cas9"; distance = 0, ambig_max = 0)
dbi = CRISPRofftargetHunter.DBInfo(genome, "hg38v34_Cas9_0_0", motif)
guides = Vector{UInt64}() # each guide is encoded
ambig = CRISPRofftargetHunter.gatherofftargets!(guides, dbi)
guides = sort(guides)
guides, counts = CRISPRofftargetHunter.ranges(guides)
# as we only care about counts
guides = nothing
counts = length.(counts)
counts = sort(counts)
count, times = CRISPRofftargetHunter.ranges(counts)
times = length.(times)

# save results
res = DataFrame(count = count, times = times)
CSV.write("hg38v34_Cas9_0_0_unique_gRNA.csv", res)
