This repository contains benchmark code for CRISPRofftargetHunter paper.

Scripts folder contains following scripts, should be run by the order indicated before script name.  

- 0_get_genome.sh - download gneome and annotations, put the into genome folder
- 0_get_top_N_genes.sh - get latest PubMed citations for each human genes and prepare top 1000 most cited genes
- 0_install.sh installs software for benchmarking.
  - For the use of CRISPRITZ docker needs to be downloaded
  - For the use of Calitas Java Runtime version >8 is needed
  - Cas-OFFinder is simply downloaded as a binary file, running this requires on OpenCL
  - This is highly system specific, and so we assume OpenCL to be present on the system running this benchmarking

