#!/bin/bash

# build genome index
cd soft/crispritz

# split fasta into multiple fasta one for each chromosome...
# seqkit split -i < ../../../../chopchop_genomes/hg38v34.fa
# mv ./stdin.split/* .

# example genome from CRISPRofftargetHunter
#docker run -v ${PWD}:/DATA -w /DATA -i pinellolab/crispritz crispritz.py index-genome fake_genome fake_genome_separated/ pamNGG.txt -bMax 3
docker run -v ${PWD}:/DATA -w /DATA -i pinellolab/crispritz crispritz.py index-genome hg38v34_ref hg38v34_fa_separated/ pamNGG.txt -bMax 0
docker run -v ${PWD}:/DATA -w /DATA -i pinellolab/crispritz crispritz.py index-genome hg38v34_ref hg38v34_fa_separated/ pamNGG.txt -bMax 3
docker run -v ${PWD}:/DATA -w /DATA -i pinellolab/crispritz crispritz.py search genome_library/NGG_3_hg38v34_ref/ pamNGG.txt guides_cr.txt guides_cr_hg.output -index -mm 3 -bDNA 3 -bRNA 3 -r -th 1

docker run -v ${PWD}:/DATA -w /DATA -i pinellolab/crispritz crispritz.py search genome_library/NGG_3_fake_genome/ pamNGG.txt guides_cr.txt guides_cr.output -index -mm 3 -bDNA 3 -bRNA 3 -r -th 6
docker run -v ${PWD}:/DATA -w /DATA -i pinellolab/crispritz crispritz.py search genome_library/NGG_3_fake_genome/ pamNGG.txt guides_neg_cr.txt guides_neg_cr.output -index -mm 3 -bDNA 3 -bRNA 3 -r -th 6


docker run -v ${PWD}:/DATA -w /DATA -i pinellolab/crispritz crispritz.py search hg19_ref/ pamNGG.txt guides/EMX1.txt emx1.hg19 -mm 4 -t -scores hg19_ref/



docker run -v ${PWD}:/DATA -w /DATA -i pinellolab/crispritz crispritz.py search genome_library/NGG_0_hg38v34_ref/ pamNGG.txt crispritz_input.txt crispritz.output -index -mm 4 -bDNA 0 -bRNA 0 -r -th 6

docker run -v ${PWD}:/DATA -w /DATA -i pinellolab/crispritz crispritz.py search genome_library/NGG_0_hg38v34_ref/ pamNGG.txt test_crispritz/EMX1.sgRNA.txt crispritz.output -index -mm 4 -bDNA 0 -bRNA 0 -r -th 6



