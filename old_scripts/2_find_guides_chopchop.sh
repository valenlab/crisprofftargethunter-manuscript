#!/bin/bash

# you have to have chopchop installed somewhere
# activate chopchop virtualenv
workon chopchop
PRJ_PATH='/home/ai/Projects/uib/crispr/crispr_offtarget_search_benchmark/'

# just chopchop test
/home/ai/Projects/uib/crispr/chopchop/chopchop.py -G hg38v34 -o temp -Target YWHAE -T 1 -M NGG --maxMismatches 3 -g20 --scoringMethod G_20 --consensusUnion

# chopchop_query needs to attach off-target table as 
# guide + guide rank + offtarget + MM  etc.

# test that CHOPCHOP is properly configured
/home/ai/Projects/uib/crispr/chopchop/chopchop_query.py --gene_names YWHAE,VWF -G hg38v34 -T 1 -M NGG --maxMismatches 3 -g20 --consensusUnion --offtargetsTable -scoringMethod G_20 -o $PRJ_PATH'soft_output/hg38/chopchop'

# run real command
/home/ai/Projects/uib/crispr/chopchop/chopchop_query.py --gene_names `cat $PRJ_PATH/genes/top1000_gene_list.txt` -G hg38v34 -T 1 -M NGG --maxMismatches 3 -g 20 --consensusUnion --offtargetsTable  --scoringMethod G_20 -o $PRJ_PATH'soft_output/hg38/chopchop'

