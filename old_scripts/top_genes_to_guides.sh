#!/bin/bash

#workon chopchop_fresh
#/home/ai/Projects/chopchops/chopchop/chopchop_query.py --gene_names YWHAE,VWF -G hg38 -T 1 -M NGG --maxMismatches 3 -g20 --scoringMethod G_20 -D 'chop:choppy@localhost/chopchop_dev' -o '/home/ai/Projects/benchmark_CRISPR_web/chopchop_output'
#/home/ai/Projects/chopchops/chopchop/chopchop_query.py --gene_names `cat /home/ai/Projects/benchmark_CRISPR_web/top1000_gene_list.txt` -G hg38 -T 1 -M NGG --maxMismatches 3 -g 20 --scoringMethod G_20 -D 'chop:choppy@localhost/chopchop_dev' -o '/home/ai/Projects/benchmark_CRISPR_web/chopchop_output'

# building db 
java -Xmx8g -jar FlashFry-assembly-1.9.2.jar index --tmpLocation ./tmp --database hg38_cas9ngg_database --reference ~/Projects/chopchops/genomes_chopchop/hg38.fa --enzyme spcas9ngg

./crisprAddGenome fasta ~/Projects/chopchops/genomes_chopchop/hg38.fa --desc 'hg38|hg38|hg38|human genome' --geneTable ~/Projects/chopchops/genomes_chopchop/isoforms/hg38.gene_table --baseDir ~/Projects/benchmark_CRISPR_web/Soft/crisporWebsite/genomes

#workon crispor
for i in {1..135}
do
   echo $i
   java -Xmx8g -jar ./Soft/flashfry/FlashFry-assembly-1.9.2.jar discover --database "./Soft/flashfry/hg38_cas9ngg_database" --fasta "./guides/genes_chunk_${i}.fa" \
      --output "./results/flashfry/flashfry_${i}.output"
   ./Soft/crisporWebsite/crispor.py hg38 "./guides/genes_chunk_${i}.fa" --noEffScores -o "./results/crispor/offtargets_${i}.tsv"  "./results/crispor/crispor_${i}.tsv"
done

# casofffinder
~/Soft/cas-offinder/cas-offinder casoff_input.txt G1 ./results/casoff.output


# test that on daniorerio we get also a lot of wierd misamtch and no chance of having SNP issues

java -Xmx8g -jar FlashFry-assembly-1.9.2.jar index --tmpLocation ./tmp --database hg38_cas9ngg_database --reference ~/Projects/chopchops/genomes_chopchop/danRer11.fa --enzyme spcas9ngg
./crisprAddGenome fasta ~/Projects/chopchops/genomes_chopchop/danRer11.fa --desc 'danRer11|danRer11|danRer11|danio rerio' --geneTable ~/Projects/chopchops/genomes_chopchop/isoforms/danRer11.gene_table --baseDir ~/Projects/benchmark_CRISPR_web/Soft/crisporWebsite/genomes
